import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Place } from './place.model';
import { take, map, tap, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _places = new BehaviorSubject<Place[]>([
    new Place(
      'p1' ,
      'Paket Wisata Religi "Masjid kuno Cirebon"' ,
      'Wisata Religi merupakan salah satu wisata favorit di Kota Cirebon. peninggalan-peninggalan masjid kuno bersejarah merupakan spot utama dari wisata religi ini. dengan adanya ziaroh Walisongo, wisata religi masjid kuno menjadi salah satu destinasi wisata favorit setelah berziarah walisongo yaitu Sunan Gunung Jati. paket wisata religi Masjid Kuno ini mengunjungi yakni Masjid Pejlahgrahan, Abang Panjunan, Pangeran Kejaksan, Pakungwati, dan Masjid Jagabayan. Paket Wisata Religi hanya tersedia di hari Sabtu pukul 09.00 WIB. Kita mulai dari Masjid Raya At-Taqwa Kota Cirebon dengan harga Rp 50 ribu per orang. wisatawan diajak keliling ke masjid bersejarah menggunakan bus wisata Citros (Cirebon tour on bus). ',
      'https://akcdn.detik.net.id/visual/2015/07/14/b612d9d9-76ee-45e8-8cb0-2e5705c8afb1_169.jpg?w=650',
      50000,
      new Date('2019-01-01'),
      new Date('2019-12-31'),
      'abc'
    ),
    new Place(
      'p2' ,
      'wisata Cirebon' ,
      'Wisata Cirebon ini merupakan wisata budaya dan kuliner yang terdapat di Kota Cirebon. salah satu paket yang ditawarkan yaitu paket 1 day trip dan 2 day 1 night. paket paket tersebut menjumpai beberapa spot-spot wisata yang ada di kota Cirebon yaitu Keraton Kesepuhan, Keraton Kacirbonan, Keraton Kanoman, Goa Sunyaragi, dan kampung batik Trusmi. kuliner-kulinernya pun banyak yang dapat kita nikmati diantara Empal Gentong, Nasi Jamblang, Docang, Nasi lengko dan masih banyak lainnya. info lebih lanjut http://wisatacirebon.info/ Selamat Berwisata',
      'https://www.javatravel.net/wp-content/uploads/2019/04/Cirebon-Waterland-Ade-Irma-Suryani.jpg',
      100000 - 400000,
      new Date('2019-01-01'),
      new Date('2019-12-31'),
      'abc'
    ),
    new Place (
      'p3' ,
      'Wisata Kuningan' ,
      'Objek wisata alam di Kuningan terdapat Tenjo Laut Palutungan, Curug Cilengkrang, bukit Cisantana, Telaga Biru dan masih banyak lainnya. berikut link harga https://travelspromo.com/htm-wisata/curug-putri-palutungan-kuningan/ ',
      'https://travelspromo.com/wp-content/uploads/2019/04/Pesona-Keindahan-Curug-Palutungan-Tonny-Hidayat-P.jpg',
      5000 - 20000,
      new Date('2019-01-01'),
      new Date('2019-12-31'),
      'abc'
    )
  ]) ;

  get places(){
    return this._places.asObservable();
  }

  constructor(private authService: AuthService) {}

  getPlace(id: string) {
    return this.places.pipe(take(1), map(places => {
      return {...places.find(p => p.id === id) };
    }));
    
  }



  addPlace(title: string, description: string, price: number, dateFrom: Date, dateTo: Date,) {
    const newPlace = new Place(Math.random().toString(), title, description, 'https://3.bp.blogspot.com/-YFHRA1BSOrc/XFucw2mYIII/AAAAAAAAEQA/pgPNAuFBHeE9rWdcZlVfuyP8YA_fbu0MACLcBGAs/s1600/Jalan-Kalibaru-Selatan-Kota-Cirebon.-Lokasi-ini-diduga-menjadi-tempat-prostitusi.-Alwi-730x355.jpg', price, dateFrom, dateTo, this.authService.userId);
    return this.places.pipe(take(1), delay(1000), tap( places => {
          this._places.next(places.concat(newPlace));
      }));
  }
}
