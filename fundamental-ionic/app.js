const reasonInput = document.querySelector('#input-reason');
const amountInput = document.querySelector('#input-amount');
const cancelBtn = document.querySelector('#btn-cancel');
const confirmBtn = document.querySelector('#btn-confirm');
const expensesList = document.querySelector('#expenses-list');
const totalExpensesOutput = document.querySelector('#total-expenses');


let totalExpenses = 0;

const clear = () => {
    reasonInput.value = '';
    amountInput.value = '';
};


confirmBtn.addEventListener('click', handleButtonClick);

async function handleButtonClick() {
    const enteredReason = reasonInput.value;
    const enteredAmount = amountInput.value;
    
    if (
        enteredReason.trim().length <= 0 ||
        enteredAmount <= 0 ||
        enteredAmount.trim().length <= 0
    );

    const alert = await alertController.create({
        header: 'Peringatan',
        message: 'Harap Masukkan Data !!!',
        buttons: ['Okay']
    });
    
    await alert.present();
    {
        return; 
    }
    console.log(enteredReason, enteredAmount);
    const newItem = document.createElement('ion-item');
    newItem.textContent = enteredReason + ' : Rp.' + enteredAmount;

    expensesList.appendChild(newItem);

    totalExpenses += +enteredAmount;
    totalExpensesOutput.textContent = totalExpenses;
    clear();
};



cancelBtn.addEventListener('click', clear);