import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  recipes : Recipe[] = [
    {
      id : 'r1',
      title : 'Mendoan',
      imageUrl : 'https://cdn-brilio-net.akamaized.net/news/2019/10/29/173086/750xauto-5-resep-dan-cara-membuat-tempe-mendoan-enak-dan-gurih-191029y.jpg',
      ingredients : ['Tempe iris tipis/khusus mendoan','6 sdm terigu','2 sdm tepung beras']
    },
    {
      id : 'r2',
      title : 'Perkedel',
      imageUrl : 'https://img-global.cpcdn.com/recipes/54b1c0525634fdb7/751x532cq70/perkedel-kentang-foto-resep-utama.jpg',
      ingredients : ['500 gr kentang','5 btr bawang merah','5 siung bawang putih']
    },
  ];

  constructor() { }

  getAllRecipes(){
    return [...this.recipes]
  };

  getRecipe(recipeId: string){
    return {
      ...this.recipes.find(recipe => {
        return recipe.id === recipeId;
      })
    }
  }

  deleteRecipe(recipeId: string) {
    this.recipes = this.recipes.filter(recipe => {
      return recipe.id !== recipeId;
    });
  }

}
